# Stash example of creating an eventlistener component

This is an example of a plugin component that registers and listens for events. Specifically, this example listens for
the `RepositoryCreationRequestedEvent`, checks whether the repository name matches a naming policy and cancels the
repository creation if it does not match.

Key steps:


## Create the event listener
Create a class (`RepositoryNameEnforcer` in this example) and add a public void method to it that takes one argument:
the event class you're interested in. Add the `@EventListener` annotation to this method to register it with the
event system.

## Define i18n error message
When cancelling the event, we'll need to provide an internationalized message (KeyedMessage) to the UI. To create one,
we'll use the `I18nService`. Add it as a constructor argument to have the plugins framework automatically inject it
to this component.

* Choose some key for the message and add the English message to src/main/resources/stash-example-eventlistener.properties

## Wire everything together

Open src/main/resources/atlassian-plugin.xml and

* ensure that the i18n resources are registered: `<resource type="i18n" name="i18n" location="stash-example-eventlistener"/>`
* import the i18nService into your plugin: `<component-import key="i18nService" interface="com.atlassian.stash.i18n.I18nService"/>`
* declare your component to have the plugin instantiate and autowire it when the plugin is enabled: `<component key="repositoryNameEnforcer" class="com.atlassian.stash.example.eventlistener.RepositoryNameEnforcer"/>`

## Take it for a spin
Start Stash to try out the plugin

```
atlas-run
```

Point your browser to <http://localhost:7990/stash>, login with admin/admin and try to create a new repository


# Further reading
* <https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK>
* <https://developer.atlassian.com/stash/docs/latest/index.html>
