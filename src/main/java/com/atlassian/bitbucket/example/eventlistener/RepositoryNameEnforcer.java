package com.atlassian.bitbucket.example.eventlistener;

import com.atlassian.bitbucket.event.repository.RepositoryCreationRequestedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.bitbucket.i18n.I18nService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RepositoryNameEnforcer {

    private static final Pattern REPO_NAME_PATTERN = Pattern.compile("[a-z0-9]*");

    private final I18nService i18nService;

    public RepositoryNameEnforcer(I18nService i18nService) {
        this.i18nService = i18nService;
    }

    @EventListener
    public void onRepoCreationRequested(RepositoryCreationRequestedEvent event) {
        Matcher matcher = REPO_NAME_PATTERN.matcher(event.getRepository().getName());
        if (!matcher.matches()) {
            event.cancel(i18nService.createKeyedMessage("enforce.invalid.repository.name",
                    event.getRepository().getName()));
        }
    }
}
