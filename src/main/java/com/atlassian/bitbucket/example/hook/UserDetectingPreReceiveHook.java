package com.atlassian.bitbucket.example.hook;

import javax.annotation.Nonnull;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.stream.Collectors;

public class UserDetectingPreReceiveHook implements PreReceiveRepositoryHook
{
    private static final Logger log = LoggerFactory.getLogger(UserDetectingPreReceiveHook.class);

    private final AuthenticationContext authenticationContext;

    public UserDetectingPreReceiveHook(AuthenticationContext authenticationContext)
    {
        this.authenticationContext = authenticationContext;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext repositoryHookContext, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse hookResponse)
    {
        ApplicationUser user = authenticationContext.getCurrentUser();
        String username = user == null ? "<anonymous>" : user.getDisplayName();
        log.info("{} pushed changes to {}", username, refChanges.stream().map(change -> change.getRef().getId()).collect(Collectors.joining(",")));
        return true;
    }
}
