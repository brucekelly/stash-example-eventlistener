package com.atlassian.bitbucket.example.hook;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.PreReceiveHook;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.ssh.SshKey;
import com.atlassian.bitbucket.ssh.SshKeyService;
import com.atlassian.bitbucket.user.AbstractApplicationUserVisitor;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.ServiceUser;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class AuthAuditHook implements PreReceiveHook {

    private static final Logger log = LoggerFactory.getLogger(AuthAuditHook.class);
    private AuthenticationContext authContext;
    private SshKeyService keyService;

    public AuthAuditHook(AuthenticationContext authContext, SshKeyService keyService) {
        this.authContext = authContext;
        this.keyService = keyService;
    }

    @Override
    public boolean onReceive(Repository repository, Collection<RefChange> changed,  HookResponse response) {

        ApplicationUser user = authContext.getCurrentUser();
        if (user != null) {
            user.accept(new AbstractApplicationUserVisitor<Void>() {
                @Override
                public Void visit(ServiceUser user) {
                    if ("access-key".equals(user.getLabel())) {
                        Page<SshKey> keys = keyService.findAllForUser(user, new PageRequestImpl(0, 10));
                        if (keys.getSize() > 1) {
                            log.info("Auth by access key user '{}' '{}'. Multiple keys possible..", user.getId(), user.getDisplayName());
                        } else {
                            SshKey key = keys.getValues().iterator().next();
                            log.info("Auth by access key user '{}' '{}'. key id {}", user.getId(), user.getDisplayName(),
                                    key.getId());
                        }
                    }
                    return null;
                }

                @Override
                public Void visit(ApplicationUser user) {
                    log.info("Auth by user '{}'", user.getName());
                    return super.visit(user);
                }
            });
        } else {
            // No way to record SSH key ID of project/repository access key (granted write access to
            // repositories)
            log.debug("Audit log : SSH key used by anonymous user cannot be identified");
        }

        return true;
    }
}
